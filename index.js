

var elem = document.createElement("div");
var rootId = '_zzz';
var rootIdSelector = '#_zzz';
elem.id = rootId;
document.body.insertBefore(elem,document.body.childNodes[0]);


var mainIframe = "<iframe id='_mainIframePortal' style='display:none'></iframe>";

function numberFormat(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function load_template(){
    var con = document.querySelectorAll(rootIdSelector)[0];
    var xhr = new XMLHttpRequest();
    xhr.open("GET", chrome.extension.getURL("../template/index.html"), true);
    xhr.setRequestHeader('Content-type', 'text/html');
    xhr.send();
    xhr.onreadystatechange = function(e) {
        if(xhr.readyState == 4 && xhr.status == 200) {
            con.innerHTML = xhr.responseText+mainIframe;
            init();
        }
    };
}
load_template();

/**
 * Created by CongHD on 2020/6/30.
 */

function showModal(){
    $('#_mainModal').modal({
        escapeClose: false,
        clickClose: false,
        showClose: false
    });
}
var inputData = [];

function init(){

    let current = window.location.href;
    if(current.indexOf('cws') != -1) {
        $('.time-regist').hide();
        if(document.title == '勤務実績入力（期間入力用）') {
            $('#_next_btn').hide();
            $('.div-time-input').show();
            if($('#APPROVALGRD').html() != null) {
                $('#_next_page').hide();
                $('#_not_yet_input').show();
                $('#_check_first_15').show();
                $('#_check_all').show();
            } else {
                $('#_next_page').show();
                $('#_not_yet_input').hide();
                $('#_check_first_15').hide();
                $('#_check_all').hide();
            }
        } else {
            $('#_next_btn').show();
            $('.div-time-input').hide();
        }
    } else {
        $('.task-regist').hide();
    }

    $(document).on('click', '#_auto_fill', function () {
        $("tr.highlight-row").map(function(){
            $item=$(this);
            $checkRedDay=$item.find("td:eq(0)").find("span");
            if($checkRedDay.css("color")!="rgb(0, 0, 0)"){
                $item.find("td:eq(1)").find(":radio[value=NULL]").attr('checked',true);
            } else {
                let shushaTime = $('input[name="shusha_time"]').val();
                let taishaTime = $('input[name="taisha_time"]').val();
                let kyukeiStart = $('input[name="kyukei_start"]').val();
                let kyukeiEnd = $('input[name="kyukei_end"]').val();

                $item.find("td:eq(1)").find(":radio[value=FREE]").attr('checked',true);
                $item.find("td:eq(1)").find(":text[name=f42]").val(shushaTime);
                $item.find("td:eq(1)").find(":text[name=f43]").val("00");
                $item.find("td:eq(1)").find(":text[name=f44]").val(taishaTime);
                $item.find("td:eq(1)").find(":text[name=f45]").val("00");
                $item.find("td:eq(1)").find(":text[name=f46]").val(kyukeiStart);
                $item.find("td:eq(1)").find(":text[name=f47]").val("00");
                $item.find("td:eq(1)").find(":text[name=f48]").val(kyukeiEnd);
                $item.find("td:eq(1)").find(":text[name=f49]").val("00");
            }
        });
    });
    $('input[name="shusha_time"]').on('keyup', function(){
        let time = parseInt($(this).val());
        $('input[name="taisha_time"]').val( time + 9);
        $('input[name="kyukei_start"]').val( time + 3);
        $('input[name="kyukei_end"]').val( time + 4);
    })
    $('input[name="kyukei_start"]').on('keyup', function(){
        let time = parseInt($(this).val());
        $('input[name="kyukei_end"]').val( time + 1);
    })
    $(document).on('click', '#_check_all', function () {
        $("#APPROVALGRD").find("tr").map(function(){
            let item=$(this).find("td").eq(0);
            item.find("input").prop('checked', true);
        });

    });

    //SELECT FIRST 15 DAYS
    $(document).on('click', '#_check_first_15', function () {
        let count = 1;
        $("input[type='checkbox']").map(function(){
            let itemName = $(this).attr('name');
            if(itemName.startsWith('CHK') && count <= 15){
                $(this).prop('checked', true);
                count++;
            }
        });
    });

    //HIDE WHEN SCROLL TO BOTTOM
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            $('#_mainForm').hide();
        } else {
            $('#_mainForm').show();
        }
    });

    $(document).on('click', '#_not_yet_input', function () {
        $('input[type="button"][value="詳細"]').each(function(){
            let projectCode = $(this).parent().parent().find('td.mg_saved').eq(17);
            let holiday = $(this).parent().parent().find('td').eq(0);
            if(projectCode.html() == null && holiday.attr('style') == undefined) {
                $(this).click();
                return false;
            }
        });
    });
    jQuery(document).ready(function ($) {
        $(document).on('click', '#_next_btn', function () {
            let pageTitle = document.title;
            if(pageTitle == 'メインメニュー') {
                $('a').each(function(){
                    let aText = $(this).html();
                    if(aText == '就労管理') {
                        window.location.href = $(this).attr('href');
                    }
                });
            } else {
                $('a').each(function(){
                    let aText = $(this).html();
                    if(aText == '勤務実績入力（期間入力用）') {
                        window.location.href = $(this).attr('href');
                    }
                });
            }
        });
    });

    jQuery(document).ready(function ($) {
        $(document).on('click', '#_next_page', function () {
            $('#btnNext0').click();
            $('#dSave0').click();
        });
    });
}

